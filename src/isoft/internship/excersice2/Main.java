package isoft.internship.excersice2;

import java.util.*;

public class Main {

    private static final char NON_CHAR_NODE = '*';

    private static Map<Character, Integer> letterOccurrence = new HashMap<>();

    private static int treeHeight = 0;
    private static int outputLength = 0;

    public static void main(String[] args) {

        String input = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";
       // String input = "ABRRKBAARAA";

        //Put in letterOccurence Map letter - times its found in the sentence
        for (int i = 0; i < input.length(); i++) {
            Character c = input.charAt(i);

            if (letterOccurrence.get(c) == null)
                letterOccurrence.put(c, 1);
            else {
                int previousCount = letterOccurrence.get(c);
                letterOccurrence.put(c, previousCount + 1);
            }


        }
        
        //Add all nodes to the PriorityQueue
        PriorityQueue<Node> queue = new PriorityQueue<>();
        for (Map.Entry<Character, Integer> entry : letterOccurrence.entrySet()) {
            Node letterNode = new Node(entry.getValue(), entry.getKey());
            queue.add(letterNode);
        }

        Node root = null;

        //Build the huffman tree 
        while (queue.size() > 1) {
            Node leftChild = queue.poll();
            Node rightChild = queue.poll();

            Node sumNode = new Node(leftChild.count + rightChild.count,
                    NON_CHAR_NODE);

            sumNode.left = leftChild;
            sumNode.right = rightChild;
            root = sumNode;


            queue.add(sumNode);
        }

      //  printTree(root);
      
        //Start DFS with initial height 1
        DFS(root, 1);

        System.out.println("Tree height is: " + treeHeight);
        System.out.println("Encoded string length is: " + outputLength);

    }

    private static void printTree(Node root) {
        if (root == null) {
            return;
        }

        System.out.println(root);
        
        printTree(root.left);
        printTree(root.right);
    }

    //DFS with current height as parameter
    private static void DFS(Node currentNode, int height) {
    
        if(currentNode == null)
            return;

        DFS(currentNode.left, height + 1);
        DFS(currentNode.right, height + 1);

        if(height > treeHeight)
            treeHeight = height;

        if(currentNode.value == NON_CHAR_NODE)
            return;

        //If its height 3 it will have 2 bits (01, 10 or 11)
        int currentLetterEncodingLength = height - 1;

        //The length of the letter in bit * times it appears in the sentence
        int sumOfBitsInOutputEncoding = letterOccurrence.get(currentNode.value) * currentLetterEncodingLength;

        outputLength += sumOfBitsInOutputEncoding;
    }
    private static class Node implements Comparable<Node> {
    
        private int count;
        private char value;

        private Node left;
        private Node right;

        Node(int count, char value) {
            this.count = count;
            this.value = value;
        }

        @Override
        public String toString() {
            return value + " : " + count;
        }

        @Override
        public int compareTo(Node node) {
            return count - node.count;
        }
        
           @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Node node = (Node) o;
            return count == node.count &&
                    value == node.value;
        }

        @Override
        public int hashCode() {
            return Objects.hash(count, value);
        }
    }
}

